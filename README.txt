CONTENTS OF THIS FILE 
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION 
------------

Current Maintainer: Jeff L (jeff@jwyl.ca)

This module was created to preserve filenames of uploaded documents. When a file
is uploaded through Drupal, a folder is created with the filename and a
subfolder with the file's timestamp. The file is moved into this folder and
renamed to the value stored in the filename attribute.


REQUIREMENTS
------------

* File Entity (https://www.drupal.org/project/file_entity)


INSTALLATION 
------------

 * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION 
-------------

 * If you want all files to go into a specific subfolder of public:// or
   private://, you can set the variable value at
   /admin/config/media/upload_archives

 * Choose formatted datetime (YYYY-MM-DD_HH\h-MM\m-SS\s) or raw timestamp for
   folder naming convention.
